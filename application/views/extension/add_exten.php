<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
      <div class="row">
          <div class="col-sm-4  "></div>
          <div class="col-sm-4  ">
              <div class="box">
                  <div class="box-body">
                      <form action="<?php  echo base_url()."ExtensionController/CreateGroup"?>" method="POST">
                      <div class="card">
                          <div class="card-header text-center">
                              NUEVO EXTENSION
                            </div>
                            <div class="card-body">
                    <div class="form-group">
                        <label>Nombres</label>
                        <input class="form-control" type="text" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>EXTENSION</label>
                        <input class="form-control" type="text" name="exten" required>
  
                    </div>
                    <div class="form-group">
                        <label>PERMISO</label>
                        <select class="form-control" type="text" name="permit_id" required>
                            <?php foreach ($groups as $key){ ?>
                            <option value="<?php echo $key->id?>"><?php echo $key->name?></option>
                            
                            <?php } ?>
                        </select>
                    </div>
    

                </div>
                <div class="card-footer text-center">
                    <a href="<?php echo base_url('PermitController/ListGroup') ;?>" class="btn btn-success "><i
                            class="fa fa-list-alt    "></i> LISTA GROUPO</a>

                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
