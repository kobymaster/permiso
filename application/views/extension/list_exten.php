<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <a href="<?php echo base_url('ExtensionController/AddExtension') ;?>"
                            class="btn btn-app btn-primary"><i class="fa fa-plus"></i> Extension</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>GROUPO</th>
                                    <th>CELULAR</th>
                                    <th>LOCAL</th>
                                    <th>INTERNACIONAL</th>
                                    <th>EMERGENCIA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($groups) {?>
                                <?php foreach ($groups as $group) {?>
                                <tr>
                                    <td><?php echo $group->name ?></td>
                                    <td><?php echo $group->celular ?></td>
                                    <td><?php echo $group->local ?></td>
                                    <td><?php echo $group->internacional ?></td>
                                    <td><?php echo $group->emergencia ?></td>
                                </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script src="<?php echo base_url()."assets/";?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()."assets/";?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()."assets/";?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js">
</script>
<script src="<?php echo base_url()."assets/";?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url()."assets/";?>bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?php echo base_url()."assets/";?>dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()."assets/";?>dist/js/demo.js"></script>
<script>
$(function() {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})
</script>