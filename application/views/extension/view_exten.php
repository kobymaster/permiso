<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
      <div class="row">
          <div class="col-sm-4  "></div>
          <div class="col-sm-4  ">
              <div class="box">
                  <div class="box-body">
                      <form action="<?php  echo base_url()."PermitController/CreateGroup"?>" method="POST">
                      <div class="card">
                          <div class="card-header text-center">
                              NUEVO GROUPO
                            </div>
                            <div class="card-body">
                    <div class="form-group">
                        <label>Nombres</label>
                        <input class="form-control" type="text" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>CELULAR</label>
                        <select class="form-control" type="text" name="celular" required>
                            <option selected>CELULAR</option>
                            <option value="1">SI</option>
                            <option value="0">NO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>LOCAL</label>
                        <select class="form-control" type="text" name="local" required>
                            <option selected>LOCAL</option>
                            <option value="1">SI</option>
                            <option value="0">NO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>INTERNACIONAL</label>
                        <select class="form-control" type="text" name="internacional" required>
                            <option selected>LOCAL</option>
                            <option value="1">SI</option>
                            <option value="0">NO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>EMERGENCIA</label>
                        <select class="form-control" type="text" name="emergencia" required>
                            <option selected>EMERGENCIA</option>
                            <option value="1">SI</option>
                            <option value="0">NO</option>
                        </select>
                    </div>


                </div>
                <div class="card-footer text-center">
                    <a href="<?php echo base_url('PermitController/ListGroup') ;?>" class="btn btn-success "><i
                            class="fa fa-list-alt    "></i> LISTA GROUPO</a>

                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
