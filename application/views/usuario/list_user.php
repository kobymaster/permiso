<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <a href="<?php echo base_url('add_user') ;?>" class="btn btn-app btn-primary"><i
                                class="fa fa-plus"></i>
                            Extension</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>USUARIO</th>
                                    <th>COMTRASEÑA</th>
                                    <th>NOMBRES</th>
                                    <th>EMAIL</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($usuarios as $usuario) {?>
                                <tr>
                                    <td><?php echo $usuario->user ?></td>
                                    <td><?php echo $usuario->pass ?></td>
                                    <td><?php echo $usuario->full_name ?></td>
                                    <td><?php echo $usuario->email ?></td>
                                    <td> <a href="<?php echo base_url('UserController/delete/').$usuario->user_id; ?>"
                                            class="btn btn-danger"><i class="fa fa-eraser" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script src="<?php echo base_url()."assets/";?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()."assets/";?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js">
</script>
<script>
$(function() {
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})
</script>