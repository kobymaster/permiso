<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-4  "></div>
            <div class="col-sm-4  ">
                <div class="box">
                    <div class="box-body">
                        <form action="<?php  echo base_url()."UserController/save"?>" method="POST">
                            <div class="card">
                                <div class="card-header text-center">
                                    NUEVO USUARIO
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>USUARIO</label>
                                        <input class="form-control" type="text" name="user"
                                            value="<?=set_value('user')?>">
                                    </div>
                                    <div class="form-group">
                                        <label>CONTRASEÑA</label>
                                        <input class="form-control" type="text" name="pass"
                                            value="<?=set_value('pass')?>">
                                    </div>
                                    <div class="form-group">
                                        <label>NOMBRE</label>
                                        <input class="form-control" type="text" name="full_name"
                                            value="<?=set_value('full_name')?>">
                                    </div>
                                    <div class="form-group">
                                        <label>CORREO</label>
                                        <input class="form-control" type="text" name="email"
                                            value="<?=set_value('email')?>">
                                    </div>



                                </div>
                                <div class="card-footer text-center">
                                    <a href="<?php echo base_url('list_user') ;?>" class="btn btn-success "><i
                                            class="fa fa-list-alt    "></i> LISTA DE USUARIO</a>

                                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>