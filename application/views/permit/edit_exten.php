<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-4  "></div>
            <div class="col-sm-4  ">
                <div class="box">
                    <div class="box-body">
                        <form action="<?php  echo base_url('update')?>" method="POST">
                            <input type="hidden" name="txt_hidden" value="<?php  echo $exten->id; ?>">
                            <div class="card">
                                <div class="card-header text-center">
                                    NUEVO EXTENSION
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>NOMBRES</label>
                                        <input class="form-control" type="text" name="name"
                                            value="<?php  echo $exten->name; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>EXTENSION</label>
                                        <input class="form-control" type="text" name="exten"
                                            value="<?php  echo $exten->extension; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>CELULAR</label>
                                        <select class="form-control" type="text" name="celular" required>
                                            <?php if ($exten->celular == 1 ){ ?>
                                            <option selected="selected" value="1">SI</option>
                                            <?php } else{ ?>
                                            <option selected="selected" value="0">NO</option>
                                            <?php  } ?>
                                            <option value="1">SI</option>
                                            <option value="0">NO</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>LOCAL</label>
                                        <select class="form-control" type="text" name="local" required>
                                            <?php if ($exten->local == 1 ){ ?>
                                            <option selected="selected" value="1">SI</option>
                                            <?php } else{ ?>
                                            <option selected="selected" value="0"> NO</option>
                                            <?php  } ?>
                                            <option value="1">SI</option>
                                            <option value="0">NO</option>
                                        </select>
                                    </div>



                                </div>
                                <div class="card-footer text-center">
                                    <a href="<?php echo base_url('list_exten') ;?>" class="btn btn-success "><i
                                            class="fa fa-list-alt    "></i> LISTA EXTENSION</a>

                                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>