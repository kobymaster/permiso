<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
      <div class="row">
          <div class="col-sm-4  "></div>
          <div class="col-sm-4  ">
              <div class="box">
                  <div class="box-body">
                      <form action="<?php  echo base_url()."PermitController/CreateGroup"?>" method="POST">
                      <div class="card">
                          <div class="card-header text-center">
                              NUEVO EXTENSION
                            </div>
                            <div class="card-body">
                    <div class="form-group">
                        <label>NOMBRES</label>
                        <input class="form-control" type="text" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>EXTENSION</label>
                        <input class="form-control" type="text" name="exten" required>
                    </div>
                    <div class="form-group">
                        <label>CELULAR</label>
                        <select class="form-control" type="text" name="celular" required>
                            <option selected>CELULAR</option>
                            <option value="1">SI</option>
                            <option value="0">NO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>LOCAL</label>
                        <select class="form-control" type="text" name="local" required>
                            <option selected>LOCAL</option>
                            <option value="1">SI</option>
                            <option value="0">NO</option>
                        </select>
                    </div>
   


                </div>
                <div class="card-footer text-center">
                    <a href="<?php echo base_url('list_exten') ;?>" class="btn btn-success "><i
                            class="fa fa-list-alt    "></i> LISTA  EXTENSION</a>

                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
