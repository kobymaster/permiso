<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-4  "></div>
            <div class="col-sm-4  ">
                <div class="box">
                    <div class="box-body">
                        <form action="<?php  echo base_url('save')?>" method="POST">
                            <input type="hidden" name="txt_hidden" value="<?php  echo isset($extension->id)?$extension->id:""; ?>">
                            <div class="card">
                                <div class="card-header text-center">
                                    <?php echo $titulo; ?>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>NOMBRES</label>
                                        <input class="form-control" type="text" name="name"
                                            value="<?php  echo isset($extension->name)?$extension->name:""; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>EXTENSION</label>
                                        <input class="form-control" type="text" name="exten"
                                            value="<?php  echo isset($extension->extension)?$extension->extension:""; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>PASSWORD</label>
                                        <input class="form-control" type="password" name="password"
                                            value="<?php  echo isset($extension->password)?$extension->password:""; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>GRUPO</label>
                                        <select class="form-control" type="text" name="grupo" required>
                                            <?php for($i=0; count($grupos)>$i; $i++): ?>
                                            <?php $selected = ""; ?>
                                            <?php if(isset($extension->group_id)): ?>
                                                <?php ($grupos[$i]->id == $extension->group_id) ? $selected = "selected": $selected="";  ?>
                                            <?php endif; ?>
                                            <option value="<?php echo $grupos[$i]->id ?>" <?php echo $selected ?>><?php echo $grupos[$i]->nombre; ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="card-footer text-center">
                                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>