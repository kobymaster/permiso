  <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">
          <div class="row">
              <div class="col-md-3">
              </div>
              <div class="col-md-6">
                  <div class="box">
                      <div class="box-header with-border">
                          <h3 class="box-title">ADD TRONCAL</h3>

                      </div>
                      <div class="box-body">

                          <form action="<?php  echo base_url()."SistemaController/CreateTrunks"?>" method="POST">
                              <div class="form-group">
                                  <label for="">TRONCAL ID</label>
                                  <input type="text" name="carrier_id" class="form-control"
                                      value="<?=set_value('carrier_id')?>">
                              </div>
                              <div class="form-group">
                                  <label for="">TRONCAL NOMBRE</label>
                                  <input type="text" name="carrier_name" class="form-control"
                                      value="<?=set_value('carrier_name')?>">
                              </div>
                              <div class="form-group">
                                  <label for="">REGISTRO</label>
                                  <input type="text" name="registration_string" class="form-control"
                                      value="<?=set_value('registration_string')?>">
                              </div>
                              <div class="form-group">
                                  <label for="">STRING</label>
                                  <input type="text" name="globals_string" class="form-control"
                                      value="<?=set_value('globals_string')?>">
                              </div>

                              <div class="form-group">
                                  <label for="">PROTOCOLO</label>
                                  <select class="form-control" id="sel1" name="protocol">
                                      <option value="SIP">SIP</option>
                                      <option value="IAX">IAX</option>
                                  </select>
                              </div>
                              <div class="form-group">
                                  <label for="">CONFIGURACION</label>
                                  <textarea class="form-control" rows="3" placeholder="Enter ..."
                                      name="account_entry"></textarea>
                              </div>
                              <div class="form-group">
                                  <label for="">DIALPLAN</label>
                                  <textarea class="form-control" rows="3" placeholder="Enter ..."
                                      name=" dialplan_entry"></textarea>
                              </div>

                              <div>
                                  <button type="submit" class="btn btn-primary">GUADAR</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
              <div class="col-md-3">
              </div>
          </div>

      </section>
  </div>