<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">ADD TRONCAL</h3>

                    </div>
                    <div class="box-body">

                        <form action="<?php  echo base_url()."trunks_update"?>" method="POST">
                            <div class="form-group">
                                <label for="">TRONCAL ID</label>
                                <input type="text" name="carrier_id" class="form-control"
                                    value="<?php  echo $trunk->carrier_id; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">TRONCAL NOMBRE</label>
                                <input type="text" name="carrier_name" class="form-control"
                                    value="<?php  echo $trunk->carrier_name; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">REGISTRO</label>
                                <input type="text" name="registration_string" class="form-control"
                                    value="<?php  echo $trunk->registration_string; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">STRING</label>
                                <input type="text" name="globals_string" class="form-control"
                                    value="<?php  echo $trunk->globals_string; ?>">
                            </div>
                            <div class="form-group">
                                <label for="">PROTOCOLO</label>
                                <select class="form-control" id="sel1" name="protocol">
                                    <option value="SIP">SIP</option>
                                    <option value="IAX">IAX</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">CONFIGURACION</label>
                                <textarea class="form-control" rows="3" placeholder="Enter ..."
                                    name="account_entry"><?php  echo $trunk->account_entry; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">DIALPLAN</label>
                                <textarea class="form-control" rows="3" placeholder="Enter ..." name=" dialplan_entry">
                                    <?php  echo $trunk->dialplan_entry; ?></textarea>
                            </div>

                            <div>
                                <button type="submit" class="btn btn-primary">GUADAR</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            </div>
        </div>

    </section>
</div>