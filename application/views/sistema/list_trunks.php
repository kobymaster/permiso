<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?php echo base_url('add_trunks') ;?>" class="btn btn-app btn-primary"><i
                                class="fa fa-plus"></i>Troncal</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>TRONCAL</th>
                                    <th>DESCRIPCION</th>
                                    <th>PROTOCOLA</th>
                                    <th>ACTIVE</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($trunks){?>
                                <?php foreach ($trunks as $trunk) { ?>
                                <tr>
                                    <td><?php echo $trunk->carrier_id ;?></td>
                                    <td><?php echo $trunk->carrier_name ;?></td>
                                    <td><?php echo $trunk->protocol ;?></td>
                                    <td><?php echo $trunk->active ;?></td>
                                    <td>
                                        <a href="<?php echo base_url('edit_trunks/').$trunk->carrier_id; ?>"
                                            class="btn btn-primary"><i class="fa fa-edit    "></i></a>
                                        <a href="<?php echo base_url('SistemaController/delete/').$trunk->carrier_id; ?>"
                                            class="btn btn-danger"><i class="fa fa-eraser" aria-hidden="true"></i></a>
                                    </td>

                                </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js">
</script>

<script>
$(function() {
    $('#example1').DataTable()
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})
</script>