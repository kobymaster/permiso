<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h2>Grupos</h2>
                        <a href="<?php echo base_url('grupos/add') ;?>" class="btn btn-app btn-primary"><i
                                class="fa fa-plus"></i>GRUPO</a>
              
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>GRUPO</th>
                                    <th>FIJO LOCAL</th>
                                    <th>FIJO LADA</th>
                                    <th>CEL LOCAL</th>
                                    <th>CEL LADA</th>
                                    <th>EDITAR</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($grupos){ ?>
                                <?php foreach ($grupos as $group) { ?>
                                <tr>
                                    <td><?php echo $group->id ;?></td>
                                    <td><?php echo $group->nombre ;?></td>
                                    <?php echo ($group->fijo_local) ? "<td class=\"bg-yellow\"> SI</td>":"<td>NO</td>" ;?>
                                    <?php echo ($group->fijo_lada)  ? "<td class=\"bg-yellow\"> SI</td>":"<td>NO</td>" ;?>
                                    <?php echo ($group->cel_local)  ? "<td class=\"bg-yellow\"> SI</td>":"<td>NO</td>" ;?>
                                    <?php echo ($group->cel_lada)   ? "<td class=\"bg-yellow\"> SI</td>":"<td>NO</td>" ;?>
                                    <td>
                                        <a href="<?php echo base_url('grupos/edit/').$group->id; ?>" class="btn btn-primary">
                                            <i class="fa fa-edit"></i></a>
                                        <a href="<?php echo base_url('grupos/delete/').$group->id; ?>"
                                            class="btn btn-danger"><i class="fa fa-eraser" aria-hidden="true"></i></a>
                                    </td>

                                </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js">
</script>

<script>
$(function() {
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    })
})
</script>