<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
      <div class="row">
          <div class="col-sm-4  "></div>
          <div class="col-sm-4  ">
              <div class="box">
                  <div class="box-body">
                      <form action="<?php  echo base_url()."grupos/save"?>" method="POST">
                      <div class="card">
                          <div class="card-header text-center">
                              GRUPO
                            </div>
                            <div class="card-body">
                    <div class="form-group">
                        <label>Nombre</label>
                        <input class="form-control" type="text" name="name" required value="<?php echo isset($grupo[0]->nombre)? $grupo[0]->nombre:""; ?>">
                    </div>
                    <div class="form-group">
                        <label>Fijo local</label>
                        <select class="form-control" type="text" name="fijo_local" required>
                            <?php $selected_si = ""; $selected_no = ""; ?>
                            <?php ($grupo[0]->fijo_local) ? $selected_si="selected":$selected_no="selected"; ?>
                            <option value="0" <?php echo $selected_no; ?>>NO</option>
                            <option value="1" <?php echo $selected_si; ?>>SI</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Fijo LADA</label>
                        <select class="form-control" type="text" name="fijo_lada" required>
                            <?php $selected_si = ""; $selected_no = ""; ?>
                            <?php ($grupo[0]->fijo_lada) ? $selected_si="selected":$selected_no="selected"; ?>
                            <option value="0" <?php echo $selected_no; ?>>NO</option>
                            <option value="1" <?php echo $selected_si; ?>>SI</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Celular local</label>
                        <select class="form-control" type="text" name="cel_local" required>
                        <?php $selected_si = ""; $selected_no = ""; ?>
                            <?php ($grupo[0]->cel_local) ? $selected_si="selected":$selected_no="selected"; ?>
                            <option value="0" <?php echo $selected_no; ?>>NO</option>
                            <option value="1" <?php echo $selected_si; ?>>SI</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Celular LADA</label>
                        <select class="form-control" type="text" name="cel_lada" required>
                            <?php $selected_si = ""; $selected_no = ""; ?>
                            <?php ($grupo[0]->cel_lada) ? $selected_si="selected":$selected_no="selected"; ?>
                            <option value="0" <?php echo $selected_no; ?>>NO</option>
                            <option value="1" <?php echo $selected_si; ?>>SI</option>
                        </select>
                    </div>

                </div>
                <div class="card-footer text-center">
                    <input type="hidden" name="id" value="<?php echo isset($grupo[0]->id)? $grupo[0]->id:""; ?>">
                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
