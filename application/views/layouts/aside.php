<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>EXTENSION</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('list_exten') ;?>"><i class="fa fa-phone"></i>Lista de
                            Extension</a></li>li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>USUARIO</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('list_user') ;?>"><i class="fa fa-user"></i>Lista de
                            Usuarios</a></li>
                    <li><a href="<?php echo base_url('grupos') ;?>"><i class="fa fa-users"></i>Lista de
                            Grupos</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gears"></i> <span>SYSTEM SETTING</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('list_trunks') ;?>"><i class="fa fa-laptop"></i>Troncal</a></li>
                    <li><a href="<?php echo base_url('list_plan') ;?>"><i class="fa fa-file"></i>Dial Plan</a></li>li>

                </ul>
            </li>


        </ul>
    </section>
</aside>