<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GrupoController extends CI_Controller {

	function __construct()
	{
	  parent::__construct();
	  $this->load->model(array('GruposModel'));
	  $this->load->helper('url');
	}

	public function index()
	{

        $data['grupos'] = $this->GruposModel->select_all ();
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('grupos/index', $data);
        $this->load->view('layouts/footer');

	}
	public function add()
	{
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('grupos/form');
        $this->load->view('layouts/footer');
    }
    public function update($id_grupo)
    {
        $data['grupo'] = $this->GruposModel->find_by_id ($id_grupo);
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('grupos/form', $data);
        $this->load->view('layouts/footer');
    }

    public function save()
    {

        if($this->input->post('id')){
            $this->GruposModel->update_grupo($this->input->post('id'),$this->input->post('name'));
            $this->GruposModel->update_permiso($this->input->post('id'), 1, $this->input->post('fijo_local'));
            $this->GruposModel->update_permiso($this->input->post('id'), 2, $this->input->post('fijo_lada'));
            $this->GruposModel->update_permiso($this->input->post('id'), 3, $this->input->post('cel_local'));
            $this->GruposModel->update_permiso($this->input->post('id'), 4, $this->input->post('cel_lada'));
        }else{
            $id = $this->GruposModel->insert_grupo($this->input->post('name'));
            $this->GruposModel->insert_permiso($id, 1, $this->input->post('fijo_local'));
            $this->GruposModel->insert_permiso($id, 2, $this->input->post('fijo_lada'));
            $this->GruposModel->insert_permiso($id, 3, $this->input->post('cel_local'));
            $this->GruposModel->insert_permiso($id, 4, $this->input->post('cel_lada'));
        }

        redirect("grupos");

    }

    public function delete(){

    }
}