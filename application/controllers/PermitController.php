<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PermitController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	  $this->load->model(array('PermitModel', 'GruposModel'));
	  $this->load->helper('url');
	  if($this->session->userdata('loggin_id') !== TRUE){
		redirect('login');
	  }

	}
  

	 public function AddExtension()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('extension/add_permit');
		$this->load->view('layouts/footer');
	}

	public function AddGroup()
	{
		$data["titulo"] = "NUEVA EXTENSION";
		$data['grupos'] = $this->GruposModel->getList();

		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		//$this->load->view('permit/add_exten');
		$this->load->view('permit/form', $data);
		$this->load->view('layouts/footer');
	}
	public function CreateGroup()
	{
		$this->PermitModel->CreateGroup();
	
		redirect(base_url('list_exten'));
	}
	public function index()
	{
		//$data['groups']=$this->PermitModel->ListGroup();
		$data['lista']=$this->PermitModel->getList();
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('permit/list_exten',$data);
		$this->load->view('layouts/footer');
	}
	public function edit($id)
    {
		$data['titulo']    = "EDITAR EXTENSIÓN";
		$data['extension'] = $this->PermitModel->getExtensionByID($id);
		$data['grupos']    = $this->GruposModel->getList();

        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
		$this->load->view('permit/form',$data);
		$this->load->view('layouts/footer');
    }
    public function update()
    {
        $this->PermitModel->update();
        redirect(base_url('list_exten'));
    }
    public function delete($id)
    {
       $this->PermitModel->delete($id);
        redirect(base_url('list_exten'));
	}
	
	public function save()
    {

		$valores["extension"] = $this->input->post('exten');
		$valores["nombre"]    = $this->input->post('name');
		$valores["password"]  = $this->input->post('password');
		$valores["id_grupo"]     = $this->input->post('grupo');
		
		if($this->input->post('txt_hidden'))
		{
			$valores["id"]     = $this->input->post('txt_hidden');
			$this->PermitModel->update_extension($valores);
		}
		else
		{
			$this->PermitModel->insert_extension($valores);
		}
		redirect(base_url('list_exten'));
    }

}