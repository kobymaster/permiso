<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SistemaController extends CI_Controller {

function __construct()
{
parent::__construct();
$this->load->model('SistemaModel');
$this->load->helper('url');
if($this->session->userdata('loggin_id') !== TRUE){
    redirect('login');
  }

}

public function ListTrunks()
{
    $data['trunks']=$this->SistemaModel->ListTrunks();
    $this->load->view('layouts/header');
    $this->load->view('layouts/aside');
    $this->load->view('sistema/list_trunks',$data);
    $this->load->view('layouts/footer');
}
public function AddTrunks()
{
    $this->load->view('layouts/header');
    $this->load->view('layouts/aside');
    $this->load->view('sistema/add_trunks');
    $this->load->view('layouts/footer');
}
public function CreateTrunks()
{
    $this->SistemaModel->CreateTrunks();
    redirect(base_url('SistemaController/ListTrunks'));
}
public function EditTrunk($carrier_id)
{

    $data['trunk'] = $this->SistemaModel->getExtensionByID($carrier_id);
    $this->load->view('layouts/header');
    $this->load->view('layouts/aside');
    $this->load->view('sistema/edit_trunks',$data);
    $this->load->view('layouts/footer');
}
public function update()
{
    $this->SistemaModel->update();
    redirect(base_url('list_trunks'));
}
public function delete($carrier_id)
{
   $this->SistemaModel->delete($carrier_id);
    redirect(base_url('list_trunks'));
}
public function ListPlan()
{
    $data['plan']=$this->SistemaModel->ListPlan();
    $this->load->view('layouts/header');
    $this->load->view('layouts/aside');
    $this->load->view('sistema/list_plan',$data);
    $this->load->view('layouts/footer');
}
public function UpdatePlan()
{
    $this->SistemaModel->UpdatePlan();
    redirect(base_url('list_plan'));
}
}