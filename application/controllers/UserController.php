<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

	function __construct()
	{
	  parent::__construct();
	  $this->load->model(array('UserModel','HomeModel'));
	$this->load->helper('url');
	if($this->session->userdata('loggin_id') !== TRUE){
		redirect('login');
	  }
  
	}
  

	public function save()
	{
		$this->UserModel->save();
		redirect(base_url('list_user'));
	}
	public function create()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('usuario/add_user');
		$this->load->view('layouts/footer');
	}
	public function index()
	{
		$data['usuarios']=$this->UserModel->index();
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('usuario/list_user',$data);
		$this->load->view('layouts/footer');
	}
	public function edit($id)
    {

        $data['exten'] = $this->UserModel->getExtensionByID($id);
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
		$this->load->view('User/edit_exten',$data);
		$this->load->view('layouts/footer');
    }
    public function update()
    {
        $this->UserModel->update();
        redirect(base_url('list_exten'));
    }
    public function delete($user_id)
    {
       $this->UserModel->delete($user_id);
        redirect(base_url('list_user'));
	}

      
	

}