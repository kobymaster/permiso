<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {


	function __construct()
	{
	  parent::__construct();
	  $this->load->model(array('PermitModel','UserModel','SistemaModel','HomeModel'));
	  $this->load->helper('url');
	}
	public function index()
	{
		 	$this->load->view('login');
	
	}
	public function home()
	{
	$data1['trunk']=$this->SistemaModel->total();

	$data2['user']=$this->UserModel->total();
	$data3['exten']=$this->PermitModel->total();
	$data = array_merge($data1,$data2,$data3);
	$this->load->view('layouts/header');
        $this->load->view('layouts/aside');
			 $this->load->view('dashboard',$data);
			 $this->load->view('layouts/footer');
	
	}
	public function login()
	{

        $result=$this->HomeModel->login();

        if ($result == 1) {
                redirect(base_url('dashboard'));
            }else
            {
				$this->load->view('login');
            }

       
      }
      public function logout()
      {
      
        $this->session->sess_destroy();
        redirect(base_url('login'));

      }
}