<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController';
$route['404_override'] = '';
//$route['translate_uri_dashes'] = FALSE;
//Configuracion de John Eder sobre las routas
//$route["home"] = 'HomeController/index/';
$route['list_exten'] = 'PermitController/index/';
$route['add_exten'] = 'PermitController/AddGroup/';
$route['add_extens'] = 'PermitController/Upload/';
$route['delete/(:any)'] = 'PermitController/delete/$1';
$route['edit/(:any)'] = 'PermitController/edit/$1';
$route['update'] = 'PermitController/update/';
$route['save'] = 'PermitController/save/';
// sistema de configuraciones de las troncales 
$route['list_trunks'] = 'SistemaController/ListTrunks/';
$route['add_trunks'] = 'SistemaController/AddTrunks/';
$route['edit_trunks/(:any)'] = 'SistemaController/EditTrunk/$1';
$route['edit_setting'] = 'SistemaController/EditSeting/';
$route['trunks_update'] = 'SistemaController/update/';
//Dialplan
$route['list_plan'] = 'SistemaController/ListPlan/';
$route['edit_plan/(:any)'] = 'SistemaController/EditPlan/$1';
$route['update_plan'] = 'SistemaController/UpdatePlan/';

$route['add_user'] = 'UserController/create/';
$route['list_user'] = 'UserController/index/';
$route['edit_user/(:any)'] = 'UserController/edit/$1';
$route['user_update'] = 'UserController/update/';
$route['show_user/(:any)'] = 'UserController/show/$1';

$route['login'] = 'HomeController/login/';
$route['logout'] = 'HomeController/logout/';
$route['dashboard'] = 'HomeController/home/';
//GRUPOS
$route['grupos'] = 'GrupoController/index/';
$route['grupos/add'] = 'GrupoController/add/';
$route['grupos/edit/(:any)'] = 'GrupoController/update/$1';
$route['grupos/save'] = 'GrupoController/save/';

