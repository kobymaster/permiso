<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class SistemaModel extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

 public function reload()
{
          $fecha =  date("Y-m-d H:i:s");
          $data_setting = array(
              'reload_timestamp'=> $fecha
          );
          $data_server = array(
              'rebuild_conf_files' =>'Y',
              'active' => 'Y',
              'active_asterisk_server' =>'Y',
              'generate_vicidial_conf' => 'Y'
          );
          $this->db->update('servers',$data_server);
          $this->db->update('system_settings',$data_setting);
          if ($this->db->affected_rows() > 0) {
            return true;
        }
        else
        {
            return false ;
        }
}
public function CreateTrunks()
{
    $datosExtention = array(
        'carrier_id' =>$this->input->post('carrier_id'),
        'carrier_name' =>$this->input->post('carrier_name'),
        'registration_string' =>$this->input->post('registration_string'),
         'account_entry' =>$this->input->post('account_entry'),
         'protocol' =>$this->input->post('protocol'),
         'dialplan_entry' =>$this->input->post('dialplan_entry'),
         'server_ip' =>'0.0.0.0',
    );
    $this->reload();
    $this->db->insert('vicidial_server_carriers',$datosExtention);
    if ($this->db->affected_rows()>0) {
        return true;
    }else
    {
        return false ;
    }
}
public function ListTrunks()
{
    $query =  $this->db->get('vicidial_server_carriers');
    if($query->num_rows() > 0){
         return $query->result();
    }else{
        return false;
    }
}
public function delete($carrier_id)
{
    $this->reload();
    $this->db->where('carrier_id',$carrier_id);
    $this->db->delete('vicidial_server_carriers');
    if( $this->db->affected_rows() > 0){
        return true;
    }else{
        return false ;
    }
}
public function update()
{
    $carrier_id = $this->input->post('carrier_id');
    $datosExtention = array(
        'carrier_id' =>$this->input->post('carrier_id'),
        'carrier_name' =>$this->input->post('carrier_name'),
        'registration_string' =>$this->input->post('registration_string'),
         'account_entry' =>$this->input->post('account_entry'),
         'protocol' =>$this->input->post('protocol'),
         'dialplan_entry' =>$this->input->post('dialplan_entry'),
         'server_ip' =>'0.0.0.0',
    );
    $this->reload();
    $this->db->where('carrier_id',$carrier_id);
    $this->db->update('vicidial_server_carriers',$datosExtention);
    if ($this->db->affected_rows() > 0) {
        return true;
    }
    else
    {
        return false ;
    }

}
public function getExtensionByID($carrier_id)
{
    $this->db->where('carrier_id',$carrier_id);
    
    $query =  $this->db->get('vicidial_server_carriers');
    if ($query->num_rows()>0) {
        return $query->row();
    }else{
        return false;
    }
}

public function ListPlan()
{
    $query =  $this->db->get('system_settings');
    if($query->num_rows() > 0){
         return $query->row();
    }else{
        return false;
   
}
}
public function UpdatePlan()
{
    $data_setting = array(
   
        'custom_dialplan_entry'=>$this->input->post('custom_dialplan_entry')
    );
    $this->reload();
    $this->db->update('system_settings',$data_setting);
    if ($this->db->affected_rows() > 0) {
        return true;
    }
    else
    {
        return false ;
    }

}
public function total()
{
    //$query =$this->db->select('*')->from('vicidial_server_carriers');
    
    //$query =  $this->db->get('vicidial_server_carriers');
    $this->db->from('vicidial_server_carriers');
   return  $this->db->count_all_results();
 
}
}