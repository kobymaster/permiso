<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class GruposModel extends CI_Model{
    public function __construct()
    {
        $this->load->library('session');
    }

    public function select_all()
    {
        $query = "select 
                  id_grupo as id,
                  nombre,
                  ft_permiso_grupo(id_grupo,1) as fijo_local, 
                  ft_permiso_grupo(id_grupo,2) as fijo_lada,
                  ft_permiso_grupo(id_grupo,3) as cel_local,
                  ft_permiso_grupo(id_grupo,4) as cel_lada
                  from tb_grupo;";
        $rs = $this->db->query($query);
        $resultado = $rs->result();
        $rs->free_result ();
        return $resultado;

    }

    public function find_by_id($id_grupo)
    {
        $query = "select 
                  id_grupo as id,
                  nombre,
                  ft_permiso_grupo(id_grupo,1) as fijo_local, 
                  ft_permiso_grupo(id_grupo,2) as fijo_lada,
                  ft_permiso_grupo(id_grupo,3) as cel_local,
                  ft_permiso_grupo(id_grupo,4) as cel_lada
                  from tb_grupo where id_grupo = " . $id_grupo . ";";
        $rs = $this->db->query($query);
        $resultado = $rs->result();
        $rs->free_result ();
        return $resultado;

    }

    public function insert_grupo($nombre)
    {
        $query = "INSERT INTO tb_grupo
                  (nombre)
                  VALUES('$nombre');";
        $this->db->query($query);
        return $this->db->insert_id();
    }

    public function update_grupo($id, $nombre)
    {
        $query = "UPDATE tb_grupo
                  SET nombre='$nombre'
                  WHERE id_grupo=$id;";
        $this->db->query($query);
    }

    public function insert_permiso($id_grupo, $id_permiso, $estatus){
        $query = "INSERT INTO tb_grupo_permiso
                  (id_grupo, id_permiso, activo)
                  VALUES(" . $id_grupo . ", " . $id_permiso . ", " . $estatus . ")";
        $this->db->query($query);

    }

    public function update_permiso($id_grupo, $id_permiso, $estatus){
        $query = "UPDATE tb_grupo_permiso
                  SET activo=" . $estatus . "
                  WHERE id_grupo=" . $id_grupo . " and id_permiso=" . $id_permiso . ";";
        $this->db->query($query);
    }

    public function getList()
    {
        $query = "select 
                  id_grupo as id,
                  nombre
                  from tb_grupo;";
        $rs = $this->db->query($query);
        $resultado = $rs->result();
        $rs->free_result ();
        return $resultado;

    }


}