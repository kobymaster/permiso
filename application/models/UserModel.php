<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class UserModel extends CI_Model{
    public function __construct()
    {
        
        $this->load->library('session');
    }

    public function index ()
    {
        $this->db->where('ACTIVE','Y');
        $query =  $this->db->get('vicidial_users');
        
        if($query->num_rows() > 0){
             return $query->result();
        }else{
            return false;
        }
    } 
public function save()
{
    $datosExtention = array(
        'user' =>$this->input->post('user'),
        'pass' =>$this->input->post('pass'),
        'full_name' =>$this->input->post('full_name'),
        'email' =>$this->input->post('email'),
    );

    $this->db->insert('vicidial_users',$datosExtention);
    if ($this->db->affected_rows()>0) {
        return true;
    }else
    {
        return false ;
    }
}

public function delete($user_id)
{
    $this->db->where('user_id',$user_id);
    $this->db->delete('vicidial_users');
    if( $this->db->affected_rows() > 0){
        return true;
    }else{
        return false ;
    }
}
public function update()
{
    $id = $this->input->post('txt_hidden');
    $datosExtention = array(
        'user' =>$this->input->post('name'),
        'pass' =>$this->input->post('pass'),
        'full_name' =>$this->input->post('full_name'),
        'email' =>$this->input->post('email'),
    );

    $this->db->where('id',$id);
    $this->db->update('vicidial_users',$datosExtention);
    if ($this->db->affected_rows() > 0) {
        return true;
    }
    else
    {
        return false ;
    }

}
public function show($id)
{
    $this->db->where('id',$id);
    $query =  $this->db->get('vicidial_users');
    if ($query->num_rows()>0) {
        return $query->row();
    }else{
        return false;
    }
}
public function total()
{
    //$query =$this->db->select('*')->from('vicidial_server_carriers');
    
    //$query =  $this->db->get('vicidial_server_carriers');
    $this->db->from('vicidial_users');
   return  $this->db->count_all_results();
 
}

}