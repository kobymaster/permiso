<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class PermitModel extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    
     
    }

  
public function CreateGroup()
{
    $extension = $this->input->post('exten');
    $this->db->where('extension',$extension);
    $query =  $this->db->get('tb_extension');
    echo "<pre>";
    print_r($query);
    echo "</pre>";
    //die();
    if ($query->num_rows()>0) {
        return "master";
    }else{
        $datosExtention = array(
            'name' =>$this->input->post('name'),
            'extension' =>$this->input->post('exten'),
            'celular' =>$this->input->post('celular'),
          'local' =>$this->input->post('local'),
            // 'internacional' =>$this->input->post('internacional'),
            // 'emergencia' =>$this->input->post('emergencia') 
            
        );
    
        $this->db->insert('tb_extension',$datosExtention);
        if ($this->db->affected_rows()>0) {
            return true;
        }else
        {
            return false ;
        }
    }
    
 
}
public function ListGroup()
{
    $query =  $this->db->get('tb_extension');
    if($query->num_rows() > 0){
         return $query->result();
    }else{
        return false;
    }
}
public function delete($id)
{
    $this->db->where('id',$id);
    $this->db->delete('tb_extension');
    if( $this->db->affected_rows() > 0){
        return true;
    }else{
        return false ;
    }
}
public function update()
{
    $id = $this->input->post('txt_hidden');
    $datosExtention = array(
        'name'      =>$this->input->post('name'),
        'extension' =>$this->input->post('exten'),
        'celular'   =>$this->input->post('celular'),
        'local'     =>$this->input->post('local'),
    );

    $this->db->where('id',$id);
    $this->db->update('tb_extension',$datosExtention);
    if ($this->db->affected_rows() > 0) {
        return true;
    }
    else
    {
        return false ;
    }

}
public function getExtensionByID($id)
{
    $this->db->where('id',$id);
    $query =  $this->db->get('tb_extension');
    if ($query->num_rows()>0) {
        return $query->row();
    }else{
        return false;
    }
}
public function total()
{
    //$query =$this->db->select('*')->from('vicidial_server_carriers');
    
    //$query =  $this->db->get('vicidial_server_carriers');
    $this->db->from('tb_extension');
   return  $this->db->count_all_results();
 
}

    public function getList()
    {
        $query = "select e.id, e.name as usuario_nombre, e.extension, g.nombre as grupo_nombre,
                 ft_permiso_grupo(e.group_id,1) as fijo_local,
                 ft_permiso_grupo(e.group_id,2) as fijo_lada,
                 ft_permiso_grupo(e.group_id,3) as cel_local,
                 ft_permiso_grupo(e.group_id,4) as cel_lada
                 from tb_extension as e
                 left join tb_grupo as g on g.id_grupo = e.group_id";
        $rs = $this->db->query($query);
        $resultado = $rs->result();
        $rs->free_result ();
        return $resultado;
    }

    public function update_extension(array $valores){
        $query = "UPDATE tb_extension
                  SET extension='".$valores["extension"]."', 
                      name     = '".$valores["nombre"]."', 
                      password = '".$valores["password"]."',
                      group_id = '".$valores["id_grupo"]."'
                  WHERE id='".$valores["id"]."'";
        $this->db->query($query);
    }

    public function insert_extension(array $valores){
        $query = "INSERT INTO tb_extension (extension, name, password, group_id)
                  VALUES('".$valores["extension"]."', '".$valores["nombre"]."', '".$valores["password"]."', '".$valores["id_grupo"]."')";
        $this->db->query($query);
    }
}