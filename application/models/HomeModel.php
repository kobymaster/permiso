<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model{


    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('date');
       

    }


   public function login()
   {
    $user = $this->input->post('user');
    $pass = $this->input->post('pass');
       $this->db->select('*');
       $this->db->from('vicidial_users');
       $this->db->where('user',$user);
       $this->db->where('pass',$pass);
       $this->db->limit(1);
  
         $resultado=$this->db->get();
     
             if ($resultado->num_rows() == 1) {
             $login_resultado  = $resultado->row();

             $sessions_login = array(
                'session_id' => $login_resultado->user_id,
                'session_nombres' => $login_resultado->full_name,
                'session_username' => $login_resultado->user,
                'session_email' => $login_resultado->email,
                'loggin_id' => TRUE
             );
             $this->session->set_userdata($sessions_login);
             return $login_resultado;

             return 1;
         }else{
      return 0;
         }

 

    /**/

   }
    public function logout()
    {
        $this->session->sess_destroy();
        return true;
    }

    public function getCentral()
    {
       $query =  $this->db->get('tb_central');
       if($query->num_rows() > 0){
            return $query->result();
       }else{
           return false;
       }
    }
    public function getRole()
    {
       $query =  $this->db->get('tb_roles');
       if($query->num_rows() > 0){
            return $query->result();
       }else{
           return false;
       }
    }

}